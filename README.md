Transfer investments demo
=========================

Problems
--------

- Using both Redux form state and own store state
- Relying on reinitialisation of the form to pass down store state through initialValues
- Triggering APIs or external events by listening on input events


Solutions
---------

- Keep form values in Redux form store until they are submitted
- Listen on Redux form value change actions to trigger side effects (APIs, etc)
