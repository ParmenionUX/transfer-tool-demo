import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { injectGlobal } from 'styled-components'
import { injectGlobalStyles, defaultTheme, ThemeProvider } from '@parmenion/library'

import App from './App'
import store from './store'

injectGlobalStyles(defaultTheme)

// eslint-disable-next-line
injectGlobal`
  body {
    padding: 20px 0;
  }
`

ReactDOM.render(
  <ThemeProvider name="base">
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('root'),
)
