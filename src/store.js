import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { reducer as formReducer } from 'redux-form'
import { createLogger } from 'redux-logger'

import rootReducer from './reducer'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()

const reducer = combineReducers({
  app: rootReducer,
  form: formReducer,
})

const logger = createLogger()

const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))

sagaMiddleware.run(sagas)

export default store
