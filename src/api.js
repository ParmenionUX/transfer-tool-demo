const DELAY = 1000

const DUMMY_SOURCE_PORTFOLIOS = [
  {
    id: 101,
    name: 'Source Portfolio 1 (ISA)',
    type: 'ISA',
  },
  {
    id: 102,
    name: 'Source Portfolio 2 (GIA)',
    type: 'GIA',
  },
  {
    id: 103,
    name: 'Source Portfolio 3 (GIA)',
    type: 'GIA',
  },
  {
    id: 104,
    name: 'Source Portfolio 4 (ISA)',
    type: 'ISA',
  },
]

const DUMMY_TARGET_PORTFOLIOS = [
  {
    id: 101,
    name: 'Target Portfolio 1 (ISA)',
    type: 'ISA',
  },
  {
    id: 102,
    name: 'Target Portfolio 2 (ISA)',
    type: 'ISA',
  },
  {
    id: 103,
    name: 'Target Portfolio 3 (GIA)',
    type: 'GIA',
  },
  {
    id: 104,
    name: 'Target Portfolio 4 (ISA)',
    type: 'ISA',
  },
]

const delay = (ms = DELAY) => new Promise(resolve => setTimeout(resolve, ms))

export const fetchSourcePortfolios = async () => {
  await delay()

  return {
    data: DUMMY_SOURCE_PORTFOLIOS,
  }
}

export const fetchTargetPortfolios = async () => {
  await delay()

  return {
    data: DUMMY_TARGET_PORTFOLIOS,
  }
}
