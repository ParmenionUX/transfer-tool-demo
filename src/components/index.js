import React from 'react'
import {
  Row,
  Column,
  Stacked,
  Container,
  PageHeader,
  Overlay,
  Spinner,
} from '@parmenion/library'

export * from './forms'

export const Page = props =>
  <Container>
    <Stacked spaceBetween={2}>
      <PageHeader>{props.title}</PageHeader>

      {props.children}
    </Stacked>
  </Container>

export const SideBySide = props =>
  <Row divisions={2}>
    <Column md={1}>
      {props.left}
    </Column>
    <Column md={1}>
      {props.right}
    </Column>
  </Row>

export const LoadingOverlay = () =>
  <div
    style={{
      position: 'relative',
      height: 55,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}
  >
    <Overlay isInverted />
    <div style={{ zIndex: 20, position: 'relative', top: -3 }}>
      <Spinner isInverted />
    </div>
  </div>
