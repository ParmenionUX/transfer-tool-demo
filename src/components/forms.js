import React from 'react'
import { reduxForm, Field } from 'redux-form'
import { Form, Button, Alert } from '@parmenion/library'

const renderFormField = ({ input, meta: { touched, error }, label, children, ...rest }) =>
  <Form.Field error={touched && error} label={label} {...rest}>
    {React.cloneElement(children, {
      ...input,
      checked: typeof input.value === 'boolean' || input.value === 'true'
        ? input.value
        : undefined, // needed until https://github.com/erikras/redux-form/issues/2512 gets fixed
      onChange: children.props.onChange || input.onChange,
    })}
  </Form.Field>

export const FormField = props => <Field {...props} component={renderFormField} />

export const SubmitButton = ({ pristine, submitting, ...rest }) =>
  <Button
    type={rest.type || 'primary'}
    action="submit"
    {...rest}
    isDisabled={submitting || rest.isDisabled}
  >
    {rest.children}
  </Button>

export const ServerForm = ({ name, action, error, ...props }) =>
  <Form
    {...props}
    name={name}
    method="POST"
    hiddenFields={[
      <input type="hidden" name="ACTION" value={action} />,
      <input type="hidden" name="FORM_NAME" value={name} />,
    ]}
  >
    {error != null &&
      <Alert status="danger">
        {error}
      </Alert>}

    {props.children}
  </Form>

export const serverForm = options => Component => {
  return reduxForm({
    ...options,
  })(Component)
}
