import { takeLatest, call, put } from 'redux-saga/effects'
import * as actions from '../actions'
import * as api from '../api'

function* loadSourcePortfoliosRequested() {
  const res = yield call(api.fetchSourcePortfolios)

  yield put(actions.loadSourcePortfoliosFetched(res.data))
}

function* loadTargetPortfoliosRequested() {
  const res = yield call(api.fetchTargetPortfolios)

  yield put(actions.loadTargetPortfoliosFetched(res.data))
}

export default function*() {
  yield takeLatest(actions.loadSourcePortfolios, loadSourcePortfoliosRequested)
  yield takeLatest(actions.loadTargetPortfolios, loadTargetPortfoliosRequested)
}
