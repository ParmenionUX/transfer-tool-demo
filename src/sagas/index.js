import { spawn, put, takeEvery } from 'redux-saga/effects'
import { actionTypes as formActionTypes } from 'redux-form'

import { loadSourcePortfolios, loadTargetPortfolios } from '../actions'
import apiSaga from './api'

function* loadApp() {
  yield put(loadSourcePortfolios())
}

function* formValueChange(action) {
  if (action.meta.form === 'transfer' && action.meta.field === 'sourcePortfolioId') {
    yield put(loadTargetPortfolios())
  }
}

export default function*() {
  yield spawn(loadApp)
  yield spawn(apiSaga)

  yield takeEvery(formActionTypes.CHANGE, formValueChange)
}
