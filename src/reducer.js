import { handleActions } from 'redux-actions'
import * as actions from './actions'

const INITIAL_STATE = {
  sourcePortfolios: null,
  targetPortfolios: null,
}

export default handleActions(
  {
    [actions.loadSourcePortfoliosFetched]: (state, action) => ({
      ...state,
      sourcePortfolios: action.payload,
    }),
    [actions.loadTargetPortfolios]: (state, action) => ({
      ...state,
      targetPortfolios: null,
    }),
    [actions.loadTargetPortfoliosFetched]: (state, action) => ({
      ...state,
      targetPortfolios: action.payload,
    }),
  },
  INITIAL_STATE,
)
