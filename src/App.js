import React from 'react'
import { reduxForm, formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { Form, Select, Alert } from '@parmenion/library'

import { Page, SideBySide, FormField, LoadingOverlay } from './components'
import { selectSourcePortfolios, selectTargetPortfolios } from './selectors'

export default () =>
  <Page title="Transfer investments">
    <TransferForm />
  </Page>

const FORM_NAME = 'transfer'

const TransferForm = reduxForm({
  form: FORM_NAME,
})(props =>
  <Form>
    <SideBySide
      left={
        <Form.Fields>
          <FormField label="Source portfolio" name="sourcePortfolioId">
            <SourcePortfolioSelect />
          </FormField>

          <FormField label="Target portfolio" name="targetPortfolioId">
            <TargetPortfolioSelect />
          </FormField>
        </Form.Fields>
      }
    />
  </Form>,
)

const selectFormValue = formValueSelector(FORM_NAME)

const SourcePortfolioSelect = connect(state => ({
  portfolios: selectSourcePortfolios(state),
}))(
  ({ portfolios, ...props }) =>
    portfolios == null ? <LoadingOverlay /> : <Select {...props} items={portfolios} />,
)

const TargetPortfolioSelect = connect(state => ({
  portfolios: selectTargetPortfolios(state),
  sourcePortfolioId: selectFormValue(state, 'sourcePortfolioId'),
}))(
  ({ sourcePortfolioId, portfolios, ...props }) =>
    sourcePortfolioId != null
      ? renderTargetPortfolioSelect(portfolios, props)
      : <Alert>Please select a source portfolio first</Alert>,
)

const renderTargetPortfolioSelect = (portfolios, props) =>
  portfolios == null ? <LoadingOverlay /> : <Select {...props} items={portfolios} />
