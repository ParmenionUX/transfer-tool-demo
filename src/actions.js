import { createAction } from 'redux-actions'

export const loadSourcePortfolios = createAction('LOAD_SOURCE_PORTFOLIOS_REQUESTED')
export const loadSourcePortfoliosFetched = createAction('LOAD_SOURCE_PORTFOLIOS_FETCHED')

export const loadTargetPortfolios = createAction('LOAD_TARGET_PORTFOLIOS_REQUESTED')
export const loadTargetPortfoliosFetched = createAction('LOAD_TARGET_PORTFOLIOS_FETCHED')
